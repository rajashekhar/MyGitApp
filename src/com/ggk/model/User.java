package com.ggk.model;

public class User {
	private int uid;
	private String name;
	private String loc;

	public int getUid() {
		return uid;
	}

	public void setUid(int uod) {
		this.uid = uod;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	@Override
	public String toString() {
		return "User [uod=" + uid + ", name=" + name + ", loc=" + loc + "]";
	}

}
